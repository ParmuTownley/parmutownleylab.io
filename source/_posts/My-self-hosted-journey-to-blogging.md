---
title: My self hosted journey to blogging
date: 2020-06-28 19:16:15
tags: self hosted, blogging, education, internet, censorship resistance
---

A couple months ago my friend showed me this github repo (https://github.com/awesome-selfhosted/awesome-selfhosted) which fascinated me into the idea of self-hosting. I soon discovered it's subreddit (r/selfhosted) and I am so glad that there is a very vibrant community around this.

I have been privacy oriented for about an year, but I've been a blogger for longer. I started blogging when I was a kid, somewhere at the age of 13. I've had 2 main blogs since and all of them were hosted on Blogger.com, which sadly is owned by Google. My first blog was about hacky ways by which we could run high-end mobile games on low-end devices. I would go deep into teaching how to root your android device and providing links to downloading pirated content. Believe it or not mobile games weren't all ads and pay to win in 2012.

My 2nd blog was more personal and I would generally write about philosophical and technical concepts like "Argument: Consciousness as an abstract concept". I've never really gotten any audience with that but I still liked posting. If you care you still visit it on https://parmu.ml

In the selfhosted repo I mentioned above there is a section for selfhosted blog. I had tried a few until I got success with https://hexo.io/. I had tried to port all my 2nd blog's posts here and decided continue it here, though I've decided to start a new blog instead. A self hosted one. I am so happy that the opensouce community exists. In the times of internet monopolies, censorship and surveillance, I am so glad that this community exists. I am still figuring this out. For now, this blog is hosted on GitLab, but I hope to host it on my own server someday.

Cheers!
